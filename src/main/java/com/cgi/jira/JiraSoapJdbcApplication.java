package com.cgi.jira;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JiraSoapJdbcApplication {

    public static void main(String[] args) {
        SpringApplication.run(JiraSoapJdbcApplication.class, args);
    }

}
